<?php

 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;

class DocumentsController extends Controller
{
 public function index(){
     $document = Document::all();
     return view('documents.index',['documents' => $document]);
 }

 public function create(){
    //$document = Document::create();
    return view('documents.create');
}


public function store(Request $request){
    //$document = Document::create();

    $document = new Document();
    $document->name = $request->name;
    $document->client = $request->city;
    $document->number = $request->number;
    $document->date = $request->date;
    $document->total = $request->message;
    $document->save();

    return redirect()->route('documents.index')->with('message',true);

}

public function about(){
   
    return view('about');
}


public function edit($id){
   
    $document = Document::find($id);
    return view('documents.edit',['document'=>$document]);
}


public function update($id, Request $request){
 
    $document = Document::find($id);

  
    $document->name = $request->name;
    $document->client = $request->city;
    $document->number = $request->number;
    $document->date = $request->date;
    $document->total = $request->message;
    $document->save();

    return redirect()->route('documents.index')->with('message',true);

}

 

public function delete($id, Request $request){
 
    Document::destroy($id);

    return redirect()->route('documents.index')->with('message',true);

}

}
