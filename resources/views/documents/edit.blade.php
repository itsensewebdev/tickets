@extends('layouts.app')

@section('content')
<section class="page-section masthead" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Edit ticket no. {{ $document->id }}</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                        <form action="{{ route('documents.update', ['id'=> $document->id]) }}" method="post" id="contactForm" name="sentMessage" novalidate="novalidate">
                        {{ csrf_field() }}
                        @method('PUT')
                        <div class="control-group">                     
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Your name</label>
                                    <input value="{{ $document->name }}" class="form-control" id="name" type="text" name="name" placeholder="Your name" required="required" data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">                     
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>City of competetion</label>
                                    <input value="{{ $document->client }}" class="form-control" id="client" type="text" name="city" placeholder="City of competetion" required="required" data-validation-required-message="Please enter city of your competetion start.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">                     
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Start number</label>
                                    <input value="{{ $document->number }}" class="form-control" id="number" type="text" name="number" placeholder="Your start number" required="required" data-validation-required-message="Please enter your start number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Start date</label>
                                    <input value="{{ $document->date }}" class="form-control" id="date" name="date" type="text" placeholder="Start Date" required="required" data-validation-required-message="Please enter your day of start competetion.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Message</label>
                                    <textarea class="form-control" id="message" name="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message.">{{ $document->total }}</textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <br>
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Send it</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
@endsection