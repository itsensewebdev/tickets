@extends('layouts.app')

@section('content')


        <!-- Portfolio Section-->        
        <section class="page-section portfolio masthead" id="portfolio">
            <div class="container">
                <!-- Portfolio Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Tickets list</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
               
            @if(session()->has('message'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Holy guacamole!</strong> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
           @endif
                <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Number</th>
      <th scope="col">Name</th>
      <th scope="col">City</th>
      <th scope="col">Start date</th>
      <th scope="col">Edit action</th>
    </tr>
  </thead>
  <tbody>


@foreach ($documents as $document)
    <tr>
      <th scope="row">{{ $document->id }}</th>
      <td>{{ $document->number }}</td>
      <td>{{ $document->name }}</td>
      <td>{{ $document->client }}</td>
      <td>{{ $document->date }}</td>
      <td><a href="{{ route('documents.edit',['id'=>$document->id]) }}" class="btn btn-info">Edit</a> <a href="{{ route('documents.delete',['id'=>$document->id]) }}" class="btn btn-danger">Usuń</a></td>
    </tr>
@endforeach
   
  </tbody>
</table>
           
           
           
            </div>
        </section>
@endsection
