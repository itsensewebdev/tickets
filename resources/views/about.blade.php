@extends('layouts.app')

@section('content')
        <!-- Masthead-->
        <header class="masthead bg-primary text-white text-center">
            <div class="container d-flex align-items-center flex-column">
              
            <img class="masthead-avatar mb-5" src="{{ asset('assets/img/tickets-logo.png') }}" alt="" />
                <!-- Masthead Heading-->
                <h1 class="masthead-heading text-uppercase mb-0">About tickets</h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <p class="font-weight-light mb-0">In this post, let's highlight the must-have features to keep in mind when choosing your next IT ticketing system. Then, we'll go over the best IT ticketing software options we've handpicked based on those standards. And, finally, we'll provide a list of some other popular tools IT professionals use to improve their workflow. </p>
                <!-- Masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0">Save your tickets in one place</p>
            </div>
        </header>
@endsection