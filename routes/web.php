<?php
use App\Http\Controllers\DocumentsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/documents', [DocumentsController::class,'index'])->name('documents.index');

Route::get('/documents/create', [DocumentsController::class, 'create'])->name('documents.create');

Route::get('/documents/about', [DocumentsController::class, 'about'])->name('documents.about');

Route::post('/documents/store', [DocumentsController::class, 'store'])->name('documents.store');

Route::get('/documents/edit/{id}', [DocumentsController::class, 'edit'])->name('documents.edit');

Route::put('/documents/change/{id}', [DocumentsController::class, 'update'])->name('documents.update');

Route::get('/documents/delete/{id}', [DocumentsController::class, 'delete'])->name('documents.delete'); 